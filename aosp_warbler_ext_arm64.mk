include device/qnx/warbler_ext/aosp_warbler_ext_common.mk

$(call inherit-product, device/qnx/warbler/aosp_warbler_arm64.mk)

PRODUCT_NAME := aosp_warbler_ext_arm64
PRODUCT_DEVICE := warbler_arm64
PRODUCT_MODEL := arm64 warbler external
