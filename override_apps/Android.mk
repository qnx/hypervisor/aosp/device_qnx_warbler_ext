LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_PACKAGE_NAME := QnxHideApps

LOCAL_SDK_VERSION := current

# Remove automotive specifc and test apps we don't want in the app grid from
# packages/services/Car/car_product/build/car.mk
# build/target/product/handheld_product.mk
LOCAL_OVERRIDES_PACKAGES := \
    CarFrameworkPackageStubs \
    AdasLocationTestApp \
    BugReportApp \
    NetworkPreferenceApp \
    CarTelemetryApp \
    RailwayReferenceApp \
    GarageModeTestApp \
    Calendar \
    Camera2 \
    Contacts \
    DeskClock \

$(info QNX LOCAL_OVERRIDES_PACKAGES is $(LOCAL_OVERRIDES_PACKAGES))

include $(BUILD_PACKAGE)
