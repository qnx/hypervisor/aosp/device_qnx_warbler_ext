# device_qnx_warbler_ext
This repository inherits changes from device_qnx_warbler. This "device" has a copy of EVS from packages/services/Car/cpp/evs/ and has QNX enhancements:
- Use USERPTR instead of MMAP with four gralloc buffers
- Support ARGB32 pixel format
