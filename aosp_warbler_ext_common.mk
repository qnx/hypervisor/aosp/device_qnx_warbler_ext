PRODUCT_PACKAGES += \
    QnxHideApps

# EVS Handling
ENABLE_QNX_EVS_SERVICE ?= true

ifeq ($(ENABLE_QNX_EVS_SERVICE), true)
ENABLE_EVS_SERVICE ?= false
ENABLE_MOCK_EVSHAL ?= false
ENABLE_EVS_SAMPLE ?= false
ENABLE_SAMPLE_EVS_APP ?= false
ENABLE_CAREVSSERVICE_SAMPLE ?= false
CUSTOMIZE_EVS_SERVICE_PARAMETER ?= false

# ENABLE_EVS_SERVICE replacement
PRODUCT_PACKAGES += qnxevsmanagerd

# ENABLE CUSTOMIZE_EVS_SERVICE_PARAMETER replacement
PRODUCT_COPY_FILES += \
    device/qnx/warbler_ext/evs/manager/aidl/init.qnxevs.rc:$(TARGET_COPY_OUT_SYSTEM)/etc/init/init.evs.rc

# ENABLE_EVS_SAMPLE replacement
PRODUCT_PACKAGES += qnxevs_app \
                    android.hardware.automotive.evs-qnx \
                    cardisplayproxyd
include device/qnx/warbler_ext/evs/apps/sepolicy/qnxevsapp.mk

PRODUCT_PRIVATE_SEPOLICY_DIRS += \
    device/qnx/warbler_ext/sepolicy/sepolicy_ext/private
PRODUCT_PUBLIC_SEPOLICY_DIRS += \
    device/qnx/warbler_ext/sepolicy/sepolicy_ext/public

endif #ENABLE_QNX_EVS_SERVICE
