# Selinux policies for the sample EVS application
PRODUCT_PRIVATE_SEPOLICY_DIRS += device/qnx/warbler_ext/evs/apps/sepolicy/private

ifeq ($(ENABLE_CARTELEMETRY_SERVICE), true)
PRODUCT_PRIVATE_SEPOLICY_DIRS += device/qnx/warbler_ext/evs/apps/sepolicy/cartelemetry
endif
