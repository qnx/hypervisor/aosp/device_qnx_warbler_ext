PRODUCT_MAKEFILES := \
    aosp_warbler_ext_arm64:$(LOCAL_DIR)/aosp_warbler_ext_arm64.mk

COMMON_LUNCH_CHOICES := \
    aosp_warbler_ext_arm64-eng \
    aosp_warbler_ext_arm64-userdebug
